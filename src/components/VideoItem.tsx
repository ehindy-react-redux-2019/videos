import React from 'react'
import Styled from 'styled-components'

const StyledItemDiv = Styled.div`
  display: flex !important;
  align-items: center !important;
  cursor: pointer;
`

const StyledImage = Styled.img`
  max-width: 180px !important;
  min-width: 180px !important;
`

type Props = {
  video: any,
  onVideoSelect: (video: any) => void
}

const VideoItem = (props: Props) => {
  const { video } = props
  return (
    <StyledItemDiv
      className="item"
      onClick={() => props.onVideoSelect(video)}
    >
      <StyledImage
        className="ui image"
        alt={video.snippet.title}
        src={video.snippet.thumbnails.medium.url}
      />
      <div className="content">
        <div className="header">{video.snippet.title}</div>
      </div>
    </StyledItemDiv>
  )
}

export default VideoItem