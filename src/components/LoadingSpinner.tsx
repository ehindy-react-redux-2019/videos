import React from 'react'

type IProps = {
  message?: string
}

const LoadingSpinner = (props: IProps) => {
  return (
    <div className="ui inverted active dimmer">
      <div className="ui text massive loader">
        {props.message}
      </div>
    </div>
  );
};

LoadingSpinner.defaultProps = {
  message: "Loading..."
}

export default LoadingSpinner;