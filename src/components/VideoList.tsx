import React from 'react'
import VideoItem from './VideoItem'

type Props = {
  videos: any[],
  onVideoSelect: (video: any) => void
}

const VideoList = (props: Props) => {
  const { videos } = props
  const renderedList = videos.map((video) => {
    return (
      <VideoItem
        key={video.id.videoId}
        video={video}
        onVideoSelect={props.onVideoSelect}
      />)
  })

  return (
    <div className="ui relaxed divided list">
      {renderedList}
    </div>
  )
}

export default VideoList