import axios from 'axios'
const KEY = process.env['REACT_APP_YOUTUBE_API_KEY']

export const defaultParams = {
        part: 'snippet',
        maxResults: 5,
        type: 'video',
        key: KEY
      }
export default axios.create({
  baseURL: "https://www.googleapis.com/youtube/v3"
})